PRSS Satellite Monitor 7.x-1.0, 2013-05-21
------------------------------------------
First release. Supports the following features:
  * Virtually unlimited number of receivers.
  * Optional auto-refresh.
  * Standard connection settings (http/https, port, address)
