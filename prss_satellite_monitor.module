<?php
/**
 * @file
 * Displays the PRSS satellite web interface in a Drupal site.
 */

/**
 * Implements hook_help().
 */
function prss_satellite_monitor_help($path, $arg) {
  switch ($path) {
    case "admin/help#satellite_monitor":
      return '<p>' . t("Displays operating information of PRSS Satellite Receivers.") . '</p>';
  }
}

/**
 * Implements hook_permission().
 */
function prss_satellite_monitor_permission() {
  return array(
    'view prss satellite monitor' => array(
      'title' => t('PRSS Satellite Monitor'),
      'description' => t('Monitor the PRSS Satellite Receivers.'),
    ),
    'administer prss satellite monitor' => array(
      'title' => t('Administer PRSS Satellite Monitor'),
      'description' => t('Add/remove/configure PRSS Satellite receivers.'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function prss_satellite_monitor_menu() {
  $items = array();
  $items['prss_satellite_monitor'] = array(
    'title' => 'PRSS Satellite Monitor',
    'page callback' => '_prss_satellite_monitor_display',
    'access arguments' => array('view prss satellite monitor'),
    'type' => MENU_NORMAL_ITEM,
  );
  $items['admin/prss_satellite_monitor/config'] = array(
    'type' => MENU_CALLBACK,
    'access arguments' => array('administer prss satellite monitor'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('_prss_satellite_monitor_admin_form'),
  );
  return $items;
}

/**
 * Implements hook_form().
 */
function _prss_satellite_monitor_admin_form($form_state) {
  $form = array();
  $form['number'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of satellite receivers'),
    '#default_value' => variable_get('prss_satellite_monitor_number', 0),
    '#required' => TRUE,
  );
  $form['autorefresh'] = array(
    '#type' => 'checkbox',
    '#title' => t('Auto-refresh the page every N seconds.'),
    '#default_value' => variable_get('prss_satellite_monitor_autorefresh', 1),
  );
  $form['refresh_interval'] = array(
    '#type' => 'textfield',
    '#title' => t('Auto-refresh interval in seconds.'),
    '#description' => t('This does nothing if auto-refresh is disabled.'),
    '#default_value' => variable_get('prss_satellite_monitor_refresh_interval', 30),
  );

  $i = 1;
  while (variable_get('prss_satellite_monitor_number', 0) >= $i) {
    $form["group1"] = array(
      '#type' => 'vertical_tabs',
    );
    $form["fieldset_$i"] = array(
      '#type' => 'fieldset',
      '#title' => _prss_json_variable_get("receiver_$i", "name", "Receiver $i"),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => t("Settings for Receiver $i."),
      '#group' => "group1",
    );
    $form["fieldset_$i"]["name_$i"] = array(
      '#type' => 'textfield',
      '#title' => t('Human-readable name of the receiver'),
      '#default_value' => _prss_json_variable_get("receiver_$i", 'name', "Receiver $i"),
      '#required' => TRUE,
    );
    $form["fieldset_$i"]["address_$i"] = array(
      '#type' => 'textfield',
      '#title' => t('Receiver domain/IP address'),
      '#default_value' => _prss_json_variable_get("receiver_$i", 'address', '127.0.0.1'),
      '#required' => TRUE,
    );
    $form["fieldset_$i"]["port_$i"] = array(
      '#type' => 'textfield',
      '#title' => t('Receiver web server port'),
      '#default_value' => _prss_json_variable_get("receiver_$i", 'port', 80),
      '#required' => TRUE,
    );
    $form["fieldset_$i"]["encryption_$i"] = array(
      '#type' => 'checkbox',
      '#title' => t('Use SSL/TLS'),
      '#default_value' => _prss_json_variable_get("receiver_$i", 'encryption', 0),
    );
    $form["fieldset_$i"]["model_$i"] = array(
      '#type' => 'select',
      '#title' => t('Receiver model'),
      '#options' => array(
        'sr2000pro' => t('SR2000pro'),
        'sfx2100r' => t('SFX2100R'),
        'sfx4104r' => t('SFX4104R'),
      ),
      '#default_value' => _prss_json_variable_get("receiver_$i", 'model', 'sr2000pro'),
      '#required' => TRUE,
    );
    $form["fieldset_$i"]["username_$i"] = array(
      '#type' => 'textfield',
      '#title' => t('Receiver username'),
      '#default_value' => _prss_json_variable_get("receiver_$i", 'username', 'admin'),
      '#required' => TRUE,
    );
    $form["fieldset_$i"]["password_$i"] = array(
      '#type' => 'password',
      '#title' => t('Receiver password'),
      '#description' => t('Leave blank to use the already-stored password.'),
    );

    $i++;
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

/**
 * Implements hook_form_submit().
 */
function _prss_satellite_monitor_admin_form_submit($form, &$form_state) {
  // Build the JSON variable.
  $i = 1;
  while (variable_get('prss_satellite_monitor_number', 0) >= $i) {
    // This if-statement allows us to avoid non-indexed notices.
    if ($form_state['values']['number'] <= variable_get('prss_satellite_monitor_number', 0)) {
      _prss_json_variable_set("receiver_$i", 'address', $form_state['values']["address_$i"]);
      _prss_json_variable_set("receiver_$i", 'port', $form_state['values']["port_$i"]);
      _prss_json_variable_set("receiver_$i", 'name', $form_state['values']["name_$i"]);
      _prss_json_variable_set("receiver_$i", 'model', $form_state['values']["model_$i"]);
      _prss_json_variable_set("receiver_$i", 'encryption', $form_state['values']["encryption_$i"]);
      _prss_json_variable_set("receiver_$i", 'username', $form_state['values']["username_$i"]);
      // Only save the password if there is a value in the field.
      if ($form_state['values']["password_$i"] !== '') {
        _prss_json_variable_set("receiver_$i", 'password', $form_state['values']["password_$i"]);
      }
    }
    $i++;
  }

  // Set Drupal variables.
  variable_set('prss_satellite_monitor_number', $form_state['values']['number']);
  variable_set('prss_satellite_monitor_autorefresh', $form_state['values']['autorefresh']);
  variable_set('prss_satellite_monitor_refresh_interval', $form_state['values']['refresh_interval']);

  drupal_set_message(t('Your configuration has been saved.'));
}

/**
 * Scrapes the satellite status graph from webmin on each receiver.
 *
 * @return string
 *   HTML graph of the satellite metrics.
 */
function _prss_satellite_monitor_display() {
  // Assign Drupal variables.
  $receiver_number = variable_get('prss_satellite_monitor_number');
  $autorefresh = variable_get('prss_satellite_monitor_autorefresh');
  $refresh_interval = check_plain(variable_get('prss_satellite_monitor_refresh_interval'));
  $html = '';

  // Add a refreshing meta tag.
  if ($autorefresh == 1) {
    $meta = array(
      '#tag' => 'meta',
      '#attributes' => array(
        'http-equiv' => 'refresh',
        'content' => $refresh_interval,
      ),
    );
    drupal_add_html_head($meta, 'wysu_tools_refresh');
    $html .= "<h3>This page will auto-refresh every $refresh_interval seconds.</h3>";
  }

  $i = 1;
  while ($receiver_number >= $i) {
    $model = _prss_json_variable_get("receiver_$i", 'model');
    $encryption = _prss_json_variable_get("receiver_$i", 'encryption');
    $name = check_plain(_prss_json_variable_get("receiver_$i", 'name'));
    $address = check_plain(_prss_json_variable_get("receiver_$i", 'address'));
    $port = check_plain(_prss_json_variable_get("receiver_$i", 'port'));
    $username = check_plain(_prss_json_variable_get("receiver_$i", 'username'));
    $password = check_plain(_prss_json_variable_get("receiver_$i", 'password'));

    $html .= "<h2>$name</h2><div id=\"prss_satellite_monitor_$i\">";
    $protocol = ($encryption == 1) ? 'https' : 'http';

    if ($model == 'sr2000pro') {
      $url = "$protocol://$address:$port/cgi-bin/StatusBar/";
      // Create a "context" for the HTML request.
      // This is essentially a user/pass request.
      $context = stream_context_create(array(
        $protocol => array(
          'header' => "Authorization: Basic " . base64_encode("$username:$password"),
        ),
      ));
      $html .= file_get_contents($url, FALSE, $context);
      $html = _prss_satellite_monitor_alter_html($html);
    }
    else {
      if ($model == 'sfx2100r') {
        $page = '/IDC_StatusBar/update.cgi';
      }
      elseif ($model == 'sfx4104r') {
        $page = '/IDC_IDEDualDemodStatusBar/update.cgi';
      }
      $post_data = http_build_query(
        array(
          'user' => $username,
          'pass' => $password,
          // This specifies the landing URL after a successful login via POST.
          'page' => $page,
        )
      );
      // Build the URL.
      $url = "$protocol://$address:$port/session_login.cgi";
      // Create a cookie file.
      $cookie_file = tempnam('/tmp', 'prss_satellite_monitor_cookie_');
      // Create a CURL handle.
      $ch = curl_init();
      // This first set of requests simply starts a session with the receiver.
      // This must be done before you POST any data.
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
      curl_setopt($ch, CURLOPT_HEADER, FALSE);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
      curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file);
      curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
      curl_setopt($ch, CURLOPT_COOKIESESSION, TRUE);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_exec($ch);

      // This set of requests actually posts the data and returns useful HTML.
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_POST, TRUE);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
      curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file);
      curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
      curl_setopt($ch, CURLOPT_COOKIESESSION, TRUE);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
      $html .= _prss_satellite_monitor_alter_html(curl_exec($ch));

      // Clean up the cookie file.
      unlink($cookie_file);
    }
    $html .= '</div>';
    $i++;
  }

  return $html;

}
/**
 * Strips problematic HTML and Javascript from the scraped graph.
 *
 * @param string $html
 *   Problematic HTML scraped from the receviers' webmin interface.
 *
 * @return string
 *   Good HTML that can be embedded without issue.
 */
function _prss_satellite_monitor_alter_html($html) {
  $html = preg_replace('/<script.*\>/', '<!--', $html);
  $html = str_replace('</script>', '-->', $html);
  // Remove some broken images.
  $html = str_replace(' background=/images/bar_status.jpg', '', $html);
  // Hide some table elements.
  $html = str_replace('<TD width="13%" align=center valign=center ROWSPAN=2>',
    '<TD width="13%" style="display:none;" align=center valign=center ROWSPAN=2>', $html
  );
  $html = str_replace(
    '<TD ROWSPAN=2 halign=center valign=center><TABLE width=100% bgcolor=#D3D3D3><TD align=center valign=center>',
    '<TD style="display:none" ROWSPAN=2 halign=center valign=center><TABLE width=100% bgcolor=#D3D3D3><TD align=center valign=center>',
    $html
  );
  // Strip all but the listed HTML tags/
  $html = strip_tags($html, '<table><td><tr><center><i><b><br/ ><br><h2></h2><h3></h3><div></div>');

  return $html;
}

/**
 * Puts variable names and values into a JSON string that's stored by Drupal.
 *
 * @param string $receiver
 *   The receiver to which this data belongs. This is the top-level key.
 *
 * @param string $variable
 *   The name of the variable being saved.
 *
 * @param string $value
 *   The value of the variable being saved.
 */
function _prss_json_variable_set($receiver, $variable, $value) {
  $json = variable_get('prss_satellite_monitor_json');
  $json = json_decode($json, TRUE);

  $json[$receiver][$variable] = $value;
  $json = json_encode($json);
  variable_set('prss_satellite_monitor_json', $json);

  return NULL;
}

/**
 * Gets variable names and values from a JSON string that's stored by Drupal.
 *
 * @param string $receiver
 *   The receiver to which this data belongs.
 *
 * @param string $variable
 *   The name of the variable being retrieved.
 *
 * @param string $default
 *   The value of the variable if a value is not currently stored.
 */
function _prss_json_variable_get($receiver, $variable, $default = NULL) {
  $json = variable_get('prss_satellite_monitor_json');
  $json = json_decode($json, TRUE);

  if ($json[$receiver][$variable] !== NULL) {
    return $json[$receiver][$variable];
  }
  else {
    return $default;
  }
}
